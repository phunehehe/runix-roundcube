{ stdenv, fetchFromGitHub, fetchurl, config-file }:
with builtins;
let
  version = "1.1.3";
  rc_openpgpjs = fetchFromGitHub {
    owner = "qnrq";
    repo = "rc_openpgpjs";
    sha256 = "13fql8fvkv635r9bm3yhfjlfk7vyjqxxb07msz9dfmf5pq2zhaid";
    rev = "8d52a5fef7b7709d69f5af49110615a41159fbeb";
  };
in stdenv.mkDerivation {
  name = "${baseNameOf ./.}-${version}";
  src = fetchurl {
    url = "https://downloads.sourceforge.net/project/roundcubemail/roundcubemail/${version}/roundcubemail-${version}-complete.tar.gz";
    sha256 = "f0d3e0f86cded38c02e0dfb3b5b875d48dd873865593ab3759e04b5fe056a9d9";
  };
  installPhase = ''
    cd ..
    mv $sourceRoot $out
    ln --symbolic ${config-file} $out/config/config.inc.php
    ln --symbolic ${rc_openpgpjs} $out/plugins/rc_openpgpjs
  '';
}
