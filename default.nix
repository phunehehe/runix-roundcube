{ config, home, exports, lib, pkgs, ... }:
let

  inherit (config) domain;
  inherit (lib) catAttrs concatStrings mapAttrsToList;

  nginx-config-link = "${home}/nginx.conf";
  nginx-log-dir = "${home}/nginx-logs";
  roundcube = pkgs.callPackage ./roundcube.nix { inherit config-file; };
  roundcube-log-dir = "${home}/roundcube-logs";

  leDir = "${home}/letsencrypt";
  certificatePath = "${leDir}/fullchain.pem";
  keyPath = "${leDir}/key.pem";

  config-file = pkgs.writeText "config.inc.php" ''
    <?php
    $config = array();

    # FIXME: this file is put there manually
    $config['db_dsnw'] = 'sqlite:///${home}/roundcube.sqlite?mode=0646';

    $config['default_host'] = 'localhost';
    $config['default_port'] = 143;
    $config['imap_auth_type'] = 'PLAIN';
    $config['imap_force_caps'] = false;
    $config['imap_cache'] = ''';
    $config['messages_cache'] = 'db';
    $config['smtp_server'] = 'localhost';
    $config['smtp_port'] = 25;
    $config['smtp_user'] = '%u';
    $config['smtp_pass'] = '%p';
    $config['support_url'] = ''';
    $config['smtp_auth_type'] = 'PLAIN';
    $config['log_dir'] = '${roundcube-log-dir}';
    $config['session_lifetime'] = 10080;
    $config['ip_check'] = false;

    # FIXME: hard coded key
    $config['des_key'] = 'AYSeMluO51I1ThGWOEoyaY89';

    $config['product_name'] = 'Roundcube Webmail';
    $config['plugins'] = array(
      'managesieve',
      'password',
      'rc_openpgpjs',
    );
    $config['enable_spellcheck'] = false;
    $config['skin'] = 'larry';
    $config['default_charset'] = 'UTF-8';
    $config['mail_pagesize'] = 20;
    $config['addressbook_pagesize'] = 20;
    $config['prefer_html'] = false;
    $config['htmleditor'] = 0;
    $config['draft_autosave'] = 0;
    $config['preview_pane'] = true;
    $config['mime_param_folding'] = 0;
    $config['read_when_deleted'] = false;
    $config['refresh_interval'] = 600;
    $config['check_all_folders'] = true;
    $config['autoexpand_threads'] = 2;
    $config['mdn_requests'] = 2;
  '';

  nginx-config = pkgs.writeText "roundcube-nginx.conf" ''
    server {

      ${exports.nginx.listenDualExtras 443 ["ssl" "spdy"]}
      ${exports.nginx.hstsConfig}
      ${exports.nginx.staticFilesConfig}

      server_name          ${domain};
      root                 ${roundcube};
      client_max_body_size 10M;

      access_log           ${nginx-log-dir}/access.log json;
      error_log            ${nginx-log-dir}/error.log;

      ssl_certificate      ${certificatePath};
      ssl_certificate_key  ${keyPath};

      location ~ \.php$ {

        # Allow caching of email preview
        if ($arg__task = mail) {
            set $cache 1;
        }
        if ($arg__action = preview) {
            set $cache "''${cache}2";
        }
        if ($cache = 12) {
            expires 1d;
        }
        # Pragma no-cache is stupid anyway
        fastcgi_hide_header Pragma;

        try_files     $uri = 404;
        include       ${pkgs.nginx}/conf/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      }

      location / {
        index     index.php;
        try_files $uri $uri/ /index.php?$args;
      }
    }

    server {
      ${exports.nginx.listenDual 80}
      server_name ${domain};
      location ~ /.well-known {
        root ${leDir};
      }
      location / {
        return 301 https://${domain}$request_uri;
      }
    }
  '';

  hooks = pkgs.leRenewHooks exports domain ++ [
    exports.nginx.reloadCommand
  ];

in {
  options = {
    domain = lib.mkOption {
      type = lib.types.str;
    };
  };
  exports = {
    letsencrypt.certs.${domain} = leDir;
    nginx.http-include = nginx-config-link;
  };

  run = pkgs.writeBash "roundcube" ''

    for d in ${nginx-log-dir} ${roundcube-log-dir}
    do
      mkdir --parents $d
      chown www-data $d
    done

    [[ -e ${certificatePath} ]] || openssl req -x509 -nodes -batch \
      -newkey rsa \
      -out ${certificatePath} \
      -keyout ${keyPath}

    ln --force --symbolic ${nginx-config} ${nginx-config-link}
    ${exports.nginx.reloadCommand}

    exec ${pkgs.leRenew [domain] leDir hooks}
  '';
}
